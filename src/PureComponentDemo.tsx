import {useEffect, useMemo} from "react";

export default function App1() {
    useEffect(() => {
        console.log("The effect")
    })


    const [data, setData] = useMemo(() => [
        {u: 1, u1: 2, u2: 3}, {u: 1, u1: 2, u2: 3}, {u: 1, u1: 2, u2: 3}
    ], []);

    const doSomething = (e: any) => {
        
    }
    return (
        <div>
            {JSON.stringify(data)}
            <button onClick={doSomething}></button>
        </div>
    );


}

