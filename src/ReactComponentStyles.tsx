import React from "react";


export function ReactComponent() {
    return (<div></div>);
}


export class ReactClassComponent extends React.Component {
    constructor(props: any) {
        super(props);
    }

    componentDidUpdate(prevProps: Readonly<{}>, prevState: Readonly<{}>, snapshot?: any) {
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return undefined;
    }
}
