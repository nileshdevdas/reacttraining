import React from "react";
import './login.css';

/**
 * Class based Component
 */
interface mystate {
    username?: string;
    password?: string;
}

export class Login extends React.Component <{}, mystate> {
    constructor(props: any) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    render() {
        return <div>
            <div className="container">
                <p style={{color: 'red'}}>xxxxxss</p>
                <div className="row">
                    <div className="col-md m-2">
                        <input type="text" id="username" name="username" className='form-control'
                               onChange={this.myChangeHandler}></input> {this.state.username}

                    </div>
                </div>
                <div className="row">
                    <div className="col-md m-2">
                        <input type="password" onChange={this.myChangeHandler} id="password" name="password"
                               className='form-control'></input>{this.state.password}
                    </div>
                </div>
                <div className="row">
                    <div className="col-md">
                        <button type="submit" className='btn btn-success form-control' onClick={this.login}>Login
                        </button>
                    </div>
                </div>
            </div>
        </div>;
    }

    login = (e: any) => {
        console.log(this.state);
        // axios.get('http://localhost:3001/version').then((responseData) => {
        //     console.log(responseData.data)
        // }).catch((err) => {
        //     console.log("Server server issue happened ......", err)
        // });
    }

    myChangeHandler = (event: any) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }
}
