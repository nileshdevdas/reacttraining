import React, {useEffect, useState} from "react";


export default function Demo() {

    const [data, setData] = useState(0);
    const [users, setUsers] = useState({username: 'nilesh', password: 'nilesh'})
    useEffect(() => {
        document.title = data + '';
    })
    return (
        <div>
            <div>{data}</div>
            <div>{users.username}</div>
            <button onClick={() => setData(data + 1)}>++</button>
            <button onClick={() => setUsers({username: Math.random() + 'nilesh', password: 'test123'})}>Button .....
            </button>
        </div>
    )
}
