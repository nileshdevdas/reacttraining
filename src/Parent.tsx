import React from "react";
import APIService from "./APIService";
import Singleton from "./Singleton";

export class Child extends React.Component<{ handleForm: any }, {}> {
    constructor(props: any) {
        super(props);
        console.log(this.state);
    }

    render() {
        return (
            <button onClick={this.props.handleForm}></button>
        )
    }
}

export class Parent extends React.Component {
    render() {
        console.log(this.state);
        return (
            <Child handleForm={this.handleClick}></Child>
        );

    }

    handleClick = (e: any) => {
        APIService().findUser({});
        Singleton.getInstance().setUsername("nilesh devdas");
        console.log(Singleton.getInstance().getUsername());
    }
}

