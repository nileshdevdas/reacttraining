import React from "react";

export function MoviesView() {
    return (
        <div>
            <div className='card'>
                <div className='card-header'>Movies</div>
                <div className='card-body'>
                    Terminator 2: Judgment Day (also promoted as T2) is a 1991 American science fiction action film
                    produced and directed by James Cameron, who co-wrote the script with William Wisher. It stars Arnold
                    Schwarzenegger, Linda Hamilton, Robert Patrick, Edward Furlong, and Joe Morton. It is the sequel to
                    the 1984 film The Terminator, and the second installment in the Terminator franchise. It follows
                    Sarah Connor (Hamilton) and her ten-year-old son John (Furlong) as they are pursued by a new, more
                    advanced Terminator: the liquidic metal, shapeshifting T-1000 (Patrick), sent back in time to kill
                    John and prevent him from becoming the leader of the human resistance. A second, less-advanced
                    Terminator (Schwarzenegger) is also sent by the Resistance to protect John.

                    Talks of a follow-up to The Terminator arose following its release, but its development was stalled
                    because of technical limitations of the vital computer-generated imagery, and legal issues with
                    original producer Hemdale Film Corporation, who controlled half of the franchise rights. In 1990,
                    Carolco Pictures acquired the rights from Hemdale and production immediately began, with
                    Schwarzenegger, Hamilton, and Cameron returning. Principal photography began in October 1990 and
                    lasted until March 1991. Its visual effects saw breakthroughs in computer-generated imagery,
                    including the first use of natural human motion for a computer-generated character and the first
                    partially computer-generated main character.[4] At the time of its release, with a budget of $94–102
                    million, Terminator 2: Judgment Day was the most expensive film ever made.
                </div>
            </div>
        </div>
    );
}
