import React from "react";

export default function HomeView() {
    return (
        <div>
            <div className='card'>
                <div className='card-header'>HomeView</div>
                <div className='card-body'>
                    US rules out adding India or Japan to security alliance with Australia and UK
                    The US has ruled out adding India or Japan to the new trilateral security partnership with Australia
                    and Britain to meet the challenges of the 21st century in the strategic Indo-Pacific region
                </div>
            </div>
        </div>
    );
}
