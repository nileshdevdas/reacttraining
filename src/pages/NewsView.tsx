import React from "react";

export default function NewsView() {
    return (
        <div>
            <div className='card'>
                <div className='card-header'>News</div>
                <div className='card-body'>
                    News is information about current events. This may be provided through many different media: word of
                    mouth, printing, postal systems, broadcasting, electronic communication, or through the testimony of
                    observers and witnesses to events. News is sometimes called "hard news" to differentiate it from
                    soft media.

                    Common topics for news reports include war, government, politics, education, health, the
                    environment, economy, business, fashion, and entertainment, as well as athletic events, quirky or
                    unusual events. Government proclamations, concerning royal ceremonies, laws, taxes, public health,
                    and criminals, have been dubbed news since ancient times. Technological and social developments,
                    often driven by government communication and espionage networks, have increased the speed with which
                    news can spread, as well as influenced its content. The genre of news as we know it today is closely
                    associated with the newspaper.
                </div>
            </div>
        </div>
    );
}
