import React from "react";
import {Redirect, Route} from "react-router-dom";

export default class GuardedRoute extends React.Component<{ component: any, auth: any, path: any }> {
    render() {
        let {component: Component, auth, ...rest} = this.props;
        return (
            <Route {...rest} render={(props) => (
                auth === true
                    ? <Component {...props} />
                    : <Redirect to='/'/>
            )}/>
        );
    }
}
