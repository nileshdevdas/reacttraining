import React from "react";
export class Ticker extends React.Component<{}, { stockLeft: number }> {
    timerID: any;
    props = {
        refreshInterval: 5000
    }

    constructor(props: any) {
        super(props);
        console.log("constructor");

        this.state = {
            stockLeft: 1000
        }
    }

    componentDidMount() {
        this.timerID = setInterval(() => {
            this.setState({stockLeft: this.state.stockLeft - 1})
        }, this.props.refreshInterval);
    }

    render() {
        console.log("render")
        // this.setState({stockLeft: this.state.stockLeft -20});
        return (<div>Current Stock {this.state.stockLeft} This Component will refresh every {this.props.refreshInterval}</div>);
    }

    /**
     * this method gets invoked when the component is unmounted .
     * Routed your self from 1 page to another
     * Routed yourself from one view another view --> Component is bound to get unmounted so all the resource tirggered by the
     * component should be cleaned up
     */
    componentWillUnmount() {
        console.log("componentWillUnmount");
        clearInterval(this.timerID);
    }
}
