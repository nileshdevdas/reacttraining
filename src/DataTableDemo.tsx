import React from "react";

export class DataTableDemo extends React.Component<any, any> {
    users = [{username: 'nilesh', password: 'abc@1234'}, {
        username: 'test@123',
        password: '12333434'
    }, {username: '23232323', password: 'x232938293823'}];

    render() {
        console.log('xxxxx')
        return (
            <div className="container-fluid col-md-4">
                <table className='table table-striped table-dark table-bordered'>
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Password</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.users.map((user) => {
                            return <tr key={user.username}>
                                <td key={user.username}>{user.username}</td>
                                <td key={user.password}>{user.password}</td>
                            </tr>
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}
