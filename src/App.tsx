import React from 'react';
import './App.css';
import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';
import HomeView from "./pages/HomeView";
import NewsView from "./pages/NewsView";
import {MoviesView} from "./pages/MoviesView";
import {Dashboard} from "./Dashboard";

function App() {
    const links = [{label: 'Home', url: ''}, 'News', 'Movies', 'Reviews']
    return (
        <BrowserRouter>
            <nav className="navbar navbar-expand-sm bg-dark">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link to="/movies"><a className="nav-link">Movies</a></Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/news"><a className="nav-link">News</a></Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/"><a className="nav-link">Home</a></Link>
                    </li>
                </ul>
            </nav>
            <div className='container-fluid mt-4'>
                <Switch>
                    <Route path='/news'>
                        <NewsView/>
                    </Route>
                    <Route path='/dash'>
                        <Dashboard/>
                    </Route>
                    <Route path='/movies'>
                        <MoviesView/>
                    </Route>
                    <Route path=''>
                        <HomeView/>
                    </Route>
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
