import React from "react";


export class News extends React.Component {
    props = {
        title : ''
    }

    render() {
        return (<div className='card'>
            <div className='card-header'>{this.props.title}</div>
            <div className='card-body'>
                Taking baby steps to scaling peaks
                Chitrasen Sahu lost both his legs after an accident, but he has successfully climbed high peaks across
                the globe, all on his artificial limbs, writes Ejaz Kaiser.
            </div>
        </div>);
    }
}
