import * as React from "react";


export class NavigationBar extends  React.Component{
    render() {
        return (<nav className="navbar navbar-expand-sm bg-light">
            <ul className="navbar-nav">
                <li className="nav-item">
                    <a className="nav-link" href="#">Link 1</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">Link 2</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link" href="#">Link 3</a>
                </li>
            </ul>
        </nav>);
    }
}
