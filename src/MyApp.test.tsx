import React from 'react';
import {render, screen} from "@testing-library/react";
import App from "./App";

/**
 * 1. you have a checklist of what you will deliver
 * 2. you will always deliver the right thing becuase you have writen the problemstatatement in code form (Test)
 * 3. Also you will be able to get the following
 *    a) Test Report
 *    b) Automation Integration
 *    c) Coverage report
 *    d) impact analysis
 *    e) How much your work is done (Metrics)
 *
 *    what about the dependency
 *    what about the mocking of these dependencies
 */

describe('News Component Scenarios', () => {
    beforeAll(()=>{
     console.log("This test case is for before all");
    });

    afterAll(()=>{
        console.log("************************AFTER ALL TESTS ARE DONE**********************************");
    })

    beforeEach(()=>{
        console.log("this runs before every test......");
    })

    afterEach(()=>{
        console.log("Generally used to run everything after the test");
    })

    test('Must have a component for News', () => {
        render(<App></App>);
        const newsComponent = screen.getByTestId('news-component');
        expect(newsComponent).toBeInTheDocument();
    });

    test('The component must have a title  Welcome to test driven development ', () => {
        render(<App></App>);
        const newsComponent = screen.getByTestId('news-component');
        console.log(newsComponent);
        const textElement = screen.findByText("Welcome to test driven development ")
        expect(textElement).toBeInTheDocument();
    })

    test('The title background Color should be Red ', () => {
        render(<App></App>);
        const newsComponent = screen.getByTestId('news-component');
    })

    test('Should have a body inside the new to display conent with size 12', () => {
        render(<App></App>);
        const newsComponent = screen.getByTestId('news-component');
    })

    test('Should have a icon on the title-bar with image', () => {
        render(<App></App>);
        const newsComponent = screen.getByTestId('news-component');
    });
});
