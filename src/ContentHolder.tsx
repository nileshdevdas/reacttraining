import React from "react";
import './ContentHolder.css'
import {SearchComponent} from "./SearchComponent";

export class ContentHolder extends React.Component {
    render() {
        return (
            <SearchComponent />
        );
    }
}
