import React from "react";
import axios from "axios";
import './SearchComponent.css'
interface SearchData {
    searchString?: string,
    maxRecords?: number
}

export class SearchComponent extends React.Component<{}, SearchData> {
    constructor(props: any) {
        super(props);
    }

    render() {
        let theStyle = {
            background: 'red',
            color: 'white',
            padding: '10px',
            borderRadius: '10px',
            left: '120px',
            width: '100%'
        }
        return (
            <div data-testid='SearchComponent'>
                <p className='demo'>SearchName...</p>
                <form onSubmit={this.handleSubmit}>
                    <p style={theStyle}>nilesh</p>
                    <label htmlFor="searchbox"></label>
                    <input type="text" id="searchbox" name="searchString" onChange={this.handleChange}/>
                    <div></div>
                    <select name="maxRecords" onChange={this.handleChange}>
                        <option>5</option>
                        <option>10</option>
                        <option>15</option>
                        <option>20</option>
                        <option>25</option>
                    </select>
                    <input type="submit" value="Search"></input>
                </form>
            </div>
        );
    }
    /** the form now is handled in single format **/
    handleChange = (e: any) => {
        console.log(e);
        let qname = e.target.name;
        let qvalue = e.target.value;
        this.setState({
            [qname]: qvalue
        });
        console.log(this.state);
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        console.log(this.state);
        sessionStorage.setItem('secrettoken', 'xxxxxxxxxx');
        localStorage.setItem('anotherlongsecret', 'vlsecret');
        axios.post('https://4231286d-d03c-43e8-ac51-ace0373c6326.mock.pstmn.io/login', this.state).then((result) => {
            // where to go on success
            console.log("result from server ", result);
        }).catch((err) => {
            console.log("error occurred ", err);
        });
    }
}
