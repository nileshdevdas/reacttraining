export default class Singleton {
    private token: any;
    private username: any;

    static inst = new Singleton();

    private constructor() {
    }

    static getInstance(): Singleton {
        return Singleton.inst;
    }

    public setUsername(username: string) {
        this.username = username;
    }

    public setPassword(password: string) {
        this.token = password;
    }

    public getPassword(): string {
        return this.token;
    }

    public getUsername(): string {
        return this.username;
    }
}

